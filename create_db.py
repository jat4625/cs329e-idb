# beginning of create_db.py
import sqlalchemy
import json
from models import app, db, Dogs, Breeds, Shelters


def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()
    return jsn


def create_breeds():
    breeds = load_json('scraped_data/breeds.json')
    for k, v in breeds.items():
        weight_str = v['weight']['imperial']
        weights_arr = weight_str.split(' - ')
        try:
            weights = list(map(float, weights_arr))
        except:
            print(k)
            print(weight_str)
        height_str = v['height']['imperial']
        heights = list(map(float, height_str.split(' - ')))
        age_str = v['life_span']
        ages = filter(lambda x: x.isdigit(), age_str.split(' '))
        ages = list(map(int, ages))

        kwargs = {
            'id': int(k),
            'name': v['name'],
            'weight_lower': weights[0],
            'height_lower': heights[0],
            'age_lower': ages[0],
        }

        if len(weights) > 1:
            if weights[1] is not None:
                kwargs['weight_upper'] = weights[1]
            else:
                kwargs['weight_upper'] = weights[0]
        if len(heights) > 1:
            kwargs['height_upper'] = heights[1]
        if len(ages) > 1:
            kwargs['age_upper'] = ages[1]

        try:
            kwargs['breed_group'] = v['breed_group']
        except KeyError:
            pass

        try:
            kwargs['origin'] = v['origin']
        except KeyError:
            kwargs['origin'] = 'Unknown'

        try:
            kwargs['bred_for'] = v['bred_for']
        except KeyError:
            kwargs['bred_for'] = 'Unknown'

        # try to add img URLs
        for x in range(3):
            img_url = ""
            try:
                img_url = v['img_urls'][x][0]['url']
            except IndexError:
                img_url = 'https://www.petango.com/DesktopModules/Pethealth.Petango/Pethealth.Petango.DnnModules.Common/Content/no-image-dog.jpg'
            finally:
                kwargs[f'img{x+1}'] = img_url

        new_breed = Breeds(**kwargs)

        db.session.add(new_breed)
        db.session.commit()

    # Add missing breeds from API
    missing_breeds = {
        'mix': {
            'id': 1001,
            'name': 'Mix',
            'weight_lower': 1.4,
            'weight_upper': 343,
            'height_lower': 3.8,
            'height_upper': 43,
            'age_lower': 0,
            'age_upper': 29,
            'bred_for': "Everything!",
            'breed_group': "All of them :)",
            'origin': "All over the world",
            'img1': "https://cdn2.thedogapi.com/images/B1Edfl9NX_1280.jpg",
            'img2': "https://cdn2.thedogapi.com/images/Sypubg54Q_1280.jpg",
            'img3': "https://cdn2.thedogapi.com/images/ryzzmgqE7_1280.jpg",
        },
        'bmc': {
            'id': 1002,
            'name': 'Black Mouth Cur',
            'weight_lower': 35,
            'weight_upper': 60,
            'height_lower': 16,
            'height_upper': 18,
            'age_lower': 12,
            'age_upper': 18,
            'bred_for': 'Herding and hunting',
            'breed_group': 'Hound',
            'origin': 'America',
            'img1': 'https://upload.wikimedia.org/wikipedia/commons/d/d2/BlackMouthCurPortrait.jpg',
            'img2': 'https://upload.wikimedia.org/wikipedia/commons/7/70/Black_mouth_cur-10.jpg',
            'img3': 'https://cdn3-www.dogtime.com/assets/uploads/gallery/black-mouth-cur-dog-breed-pictures/black-mouth-cur-dog-breed-pictures-7.jpg',
        },

        'chihuahua': {
            'id': 77,
            'name': 'Chihuahua',
            'weight_lower': 3,
            'weight_upper': 7,
            'height_lower': 5,
            'height_upper': 10,
            'age_lower': 12,
            'age_upper': 20,
            'bred_for': 'Companionship',
            'breed_group': 'Toy',
            'origin': 'Mexico',
            'img1': 'https://images.pexels.com/photos/50718/chihuahua-sobel-dog-50718.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
            'img2': 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Chihuahue%C3%B1o_paseante.JPG/1920px-Chihuahue%C3%B1o_paseante.JPG',
            'img3': 'https://upload.wikimedia.org/wikipedia/commons/a/a9/Chihuahua2.jpg'
        },

        'collie': {
            'id': 88,
            'name': 'Collie',
            'weight_lower': 50,
            'weight_upper': 70,
            'height_lower': 22,
            'height_upper': 26,
            'age_lower': 10,
            'age_upper': 14,
            'bred_for': 'Sheep herding',
            'breed_group': 'Herding',
            'origin': 'Scotland, Northern England',
            'img1': 'https://upload.wikimedia.org/wikipedia/commons/e/e4/Border_Collie_600.jpg',
            'img2': 'https://upload.wikimedia.org/wikipedia/commons/8/85/Welsh_Sheepdog.jpg',
            'img3': 'https://images.pexels.com/photos/663573/pexels-photo-663573.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
        },

        'fcr': {
            'id': 112,
            'name': 'Flat-Coated Retriever',
            'weight_lower': 60,
            'weight_upper': 70,
            'height_lower': 22,
            'height_upper': 24.5,
            'age_lower': 10,
            'age_upper': 13,
            'bred_for': 'Water retrieving',
            'breed_group': 'Sporting',
            'origin': 'United Kingdom',
            'img1': 'https://upload.wikimedia.org/wikipedia/commons/a/aa/Flat_Coated_Retriever_-_black.jpg',
            'img2': 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Flat_Coated_Retriever_retrieving.jpg/800px-Flat_Coated_Retriever_retrieving.jpg',
            'img3': 'https://upload.wikimedia.org/wikipedia/commons/1/11/Flatcoated_Retriever_Image_001.jpg',
        }
    }

    for missing_breed in missing_breeds.values():
        db.session.add(Breeds(**missing_breed))
        db.session.commit()


def create_shelters():
    shelters = load_json('scraped_data/shelters.json')
    # Remove foster homes
    shelters.pop('Pflugerville Pets Alive Foster Homes')
    shelter_id = 0
    for k, v in shelters.items():
        kwargs = {
            'id': shelter_id,
            'name': k,
            'website': v['website'],
            'email': v['email'],
            'phone': v['phone'],
            'address': v['address'],
            'img_url': v['img_url']
        }
        new_shelter = Shelters(**kwargs)
        db.session.add(new_shelter)
        db.session.commit()
        shelter_id += 1



def create_dogs():
    dogs = load_json('scraped_data/dogs.json')
    for key, value in dogs.items():
        id = key
        name = value['name']
        breed1 = value['breed1']
        breed2 = value['breed2']
        age = value['age']
        if value['shelter'] == "Foster Homes":
            shelter = "Pflugerville Pets Alive!"
        else:
            shelter = value['shelter']
        imgs = [None, None, None]
        for x in range(len(value['img_urls'])):
            imgs[x] = value['img_urls'][x]
        size = value['size']
        gender = value['gender']
        neuter = value['spay-neuter']
        desc = value['desc']
        newBook = Dogs(id=id, name=name, age=age, shelter=shelter, img1=imgs[0], img2=imgs[1], img3=imgs[2], size=size,
                       gender=gender, breed1=breed1, breed2=breed2, neuter=neuter, desc=desc)

        # After I create the book, I can then add it to my session.
        db.session.add(newBook)
        # commit the session to my DB.
        db.session.commit()


def start():
    db.drop_all()
    db.create_all()
    create_breeds()
    create_shelters()
    create_dogs()


db.drop_all()
db.create_all()
create_breeds()
create_shelters()
create_dogs()
