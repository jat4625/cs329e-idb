from bs4 import BeautifulSoup
from selenium import webdriver
from math import ceil
from json import dump, loads
import requests


def gather_urls(soup):
    """
    Takes: page soup object
    returns: array of urls
    """
    urls = []
    dogs = soup.select('.animal-template')
    for dog in dogs:
        try:
            urls.append(dog.select('.mini-profile-link')[-1].get('href'))
        except IndexError:
            pass
    return urls


def make_breed_array():
    url = "https://api.thedogapi.com/v1/breeds/"
    headers = {'x-api-key': '3faeb81a-d180-4fe9-a902-2c13430518e4'}
    response = requests.request("GET", url, headers=headers)
    return loads(response.text)


def search_breed(breed_str):
    url = "https://api.thedogapi.com/v1/breeds/search"
    querystring = {"q": breed_str}
    headers = {'x-api-key': '3faeb81a-d180-4fe9-a902-2c13430518e4'}
    response = requests.request("GET", url, headers=headers, params=querystring)
    search_results = loads(response.text)
    return search_results


def format_breed_str(breeds_str):
    """ Take in breed string scraped from Petango and split it into two strings"""

    generic_breeds = ["Hound", "Terrier", "Retriever", "Shepherd"]
    breeds = breeds_str.split(' / ')
    breeds_cleaned = []

    for breed in breeds:
        if breed[:5] == "Mixed":
            breeds_cleaned.append("Mix")
        elif ',' in breed:
            # Get rid of commas
            no_comma = " ".join(reversed(breed.split(', ')))
            breeds_cleaned.append(no_comma)
        elif breed in generic_breeds:
            breeds_cleaned.append("Mix")
        else:
            breeds_cleaned.append(breed)
    try:
        # Remove duplicate breeds
        if breeds_cleaned[0] == breeds_cleaned[1]:
            breeds_cleaned.pop()
    except IndexError:
        pass
    if len(breeds_cleaned) == 1:
        breeds_cleaned.append(None)

    return breeds_cleaned


def get_image_url(breed_id):
    """ given a breed id return an img url as a string """
    url = "https://api.thedogapi.com/v1/images/search"
    querystring = {"breed_id": breed_id}
    headers = {'x-api-key': '3faeb81a-d180-4fe9-a902-2c13430518e4'}
    response = requests.request("GET", url, headers=headers, params=querystring)
    search_results = loads(response.text)
    return search_results


def breed_to_id(breed_str, breeds_array):
    """ Take in a breed_str and convert it to an id """
    problem_breeds = {
        "Black-Mouth Cur": "1002",
        "Bulldog": 63,
        "Smooth Collie": 88,
        "Bull Terrier": 61,
        "Long Coat Chihuahua": 77,
        "Short Coat Chihuahua": 77,
        "American Blue Heeler": 21,
        "Mix": "1001",
    }

    if breed_str in problem_breeds.keys():
        return problem_breeds[breed_str]
    elif breed_str is None:
        return None
    else:
        # Works to get exact matches
        for breed in breeds_array:
            if breed["name"] == breed_str:
                return breed["id"]
    # Catches cases where breed_str is a substr
    try:
        return search_breed(breed_str)[0]["id"]
    except IndexError:
        print(f"{breed_str} is not in the API! Manually add it to the DB")
        return None


def breeds_array_to_dict(breeds_array):
    print("Starting breed.json population, this may take a while...")
    breeds_dict = {}
    for breed in breeds_array:
        breed_id = breed["id"]
        breed.pop("id", None)

        # Get 3 image URLs for each breed
        img_urls = []
        for x in range(3):
            img_urls.append(get_image_url(breed_id))
        breed["img_urls"] = img_urls

        # Add the breed to the big dict
        breeds_dict[breed_id] = breed
        print(f"Added breed {breed['name']}")
    print("breeds.json populated")
    return breeds_dict


def scrape_page(url, dog_dict, shelter_dict, breeds_array, browser):
    browser.get(url)
    dog = BeautifulSoup(browser.page_source, 'html.parser')
    dog_id = int(dog.select('span[data-bind="text: id"]')[0].text)

    dog_dict[dog_id] = {
        "name": "",
        "breed1": 0,
        "breed2": None,
        "age": 0,
        "gender": "",
        "color": "",
        "spay-neuter": False,
        "size": "",
        "desc": "",
        "img_urls": []
    }

    # Get dog info
    data_binds = ["name", "breed", "color", "age", "gender", "spayedNeutered", "size", "memo"]
    for elem in data_binds:
        if elem == "memo":
            dog_dict[dog_id]["desc"] = dog.select(f'span[data-bind="html: {elem}"]')[0].text
        elif elem == "spayedNeutered":
            dog_dict[dog_id]["spay-neuter"] = dog.select(f'span[data-bind="text: {elem}"]')[0].text
        elif elem == "breed":
            breed_strs = format_breed_str(dog.select(f'span[data-bind="text: {elem}"]')[0].text)
            breed_ids = [breed_to_id(breed_str, breeds_array) for breed_str in breed_strs]
            dog_dict[dog_id]["breed1"] = breed_ids[0]
            dog_dict[dog_id]["breed2"] = breed_ids[1]
        else:
            dog_dict[dog_id][elem] = dog.select(f'span[data-bind="text: {elem}"]')[0].text

    # Get img urls
    thumbnails = dog.select('.thumbs-wrapper .img-responsive')
    urls = []
    for img in thumbnails:
        if img.get("data-bind") != "attr: { src: youtubeThumbnail }":
            urls.append(img.get('src'))
    urls = list(filter(lambda x: x is not None, urls))
    dog_dict[dog_id]["img_urls"] = urls

    # Clean data
    dog_dict[dog_id]["gender"] = dog_dict[dog_id]["gender"][0]

    if dog_dict[dog_id]["spay-neuter"] == "Yes":
        dog_dict[dog_id]["spay-neuter"] = True
    else:
        dog_dict[dog_id]["spay-neuter"] = False
    try:
        age_str = dog_dict[dog_id]["age"]
        age = 0
        for time in age_str.split(" "):
            unit = time[-1]
            value = int(''.join(c for c in time if c.isdigit()))
            if unit == "y":
                age += 12 * value
            elif unit == "m":
                age += value
            else:
                age += round(value/30.5)
        dog_dict[dog_id]["age"] = age

    except ValueError:
        # Age is "unknown", make it -1
        dog_dict[dog_id]["age"] = -1

    size_str = dog_dict[dog_id]["size"]
    if size_str != "Extra-large":
        size_str = size_str[0]
    else:
        size_str = "XL"
    dog_dict[dog_id]["size"] = size_str

    # Add shelter info to dog
    shelter = dog.select('.side-shelter-info')[0]
    shelter_name = shelter.select('h4')[0].text
    dog_dict[dog_id]["shelter"] = shelter_name
    print(f"Dog '{dog_dict[dog_id]['name']}' added to dogs.json")

    # Scrape shelter data if not present in shelter_dict
    if shelter_name not in shelter_dict:
        shelter_img = shelter.select('.small-shelter-logo > a > img')[0].get('src')
        shelter_info = shelter.find_all('span')
        urls = shelter.find_all('a')
        phone_number = ''.join(c for c in shelter_info[0].text if c.isdigit())
        shelter_dict[shelter_name] = {
            "img_url": shelter_img,
            "website": urls[1].text,
            "email": urls[2].text,
            "phone": phone_number,
            "address": shelter_info[1].text
        }
        print(f"Shelter '{shelter_name}' added to shelters.json")

    return dog_dict, shelter_dict


def num_pages(soup):
    "Returns int, num of pages given soup"
    num_entries = int(soup.select('span[data-bind="text: count"]')[0].text)
    return ceil(num_entries/26)


def main():
    # Initializes browser
    options = webdriver.ChromeOptions()
    options.add_argument('headless')
    browser = webdriver.Chrome(chrome_options=options)

    # Change the string below to your search query
    search_query_url = 'http://www.petango.com/pet_search_results?speciesId=1&breedId=undefined&location=78705&distance=50'
    browser.get(search_query_url)

    # Scrapes the first page and initializes urls
    soup = BeautifulSoup(browser.page_source, 'html.parser')
    urls = gather_urls(soup)

    # Gather urls
    for _page in range(num_pages(soup)-1):
        browser.find_element_by_link_text('Next').click()
        soup = BeautifulSoup(browser.page_source, 'html.parser')
        urls += gather_urls(soup)

    dog_dict = {}
    shelter_dict = {}
    breeds_dict = {}
    breeds_array = make_breed_array()

    # Scrape all urls
    for url in urls:
        dog_dict, shelter_dict = scrape_page(url, dog_dict, shelter_dict, breeds_array, browser)

    # Convert breeds_array to dict
    breeds_dict = breeds_array_to_dict(breeds_array)

    # save as JSON, checking for dir first
    with open('dogs.json', 'w') as f:
        dump(dog_dict, f)

    with open('shelters.json', 'w') as f:
        dump(shelter_dict, f)

    with open('breeds.json', 'w') as f:
        dump(breeds_dict, f)


if __name__ == "__main__":
    main()
