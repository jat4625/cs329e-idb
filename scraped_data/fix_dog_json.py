from json import load, dump


def string_to_id():
    with open("dogs.json", "r") as f:
        dog_dict = load(f)

    for k, v in dog_dict.items():
        key_changed = True
        if v["breed1"] == "mix":
            dog_dict[k]["breed1"] = 1001
        elif v["breed2"] == "mix":
            dog_dict[k]["breed2"] = 1001
        elif v["breed1"] == "bmc":
            dog_dict[k]["breed1"] = 1002
        elif v["breed2"] == "bmc":
            dog_dict[k]["breed2"] = 1002
        else:
            key_changed = False
        from pprint import PrettyPrinter
        p = PrettyPrinter(depth=8)
        if key_changed:
            print("Key updated!")
            p.pprint(dog_dict[k])

    with open("dogs.json", "w") as f:
        dump(dog_dict, f)


def id_to_str(breedid, breed_dict):
    if breedid is None:
        return None
    elif breedid == 1001:
        return "Mix"
    elif breedid == 1002:
        return "Black Mouth Cur"
    elif breedid == 77:
        return "Chihuahua"
    elif breedid == 88:
        return "Collie"
    elif breedid == 112:
        return "Flat-Coated Retriever"
    else:
        return breed_dict[str(breedid)]['name']


def breed_ids_to_strs(dog_dict, breed_dict):
    for k, v in dog_dict.items():
        error = False
        dog_dict[k]['breed1'] = id_to_str(dog_dict[k]['breed1'], breed_dict)
        dog_dict[k]['breed2'] = id_to_str(dog_dict[k]['breed2'], breed_dict)
        """
        try:
            dog_dict[k]['breed1'] = id_to_str(dog_dict[k]['breed1'], breed_dict)
            dog_dict[k]['breed2'] = id_to_str(dog_dict[k]['breed2'], breed_dict)
        except KeyError:
            print(k)
            error = True
        if error:
            raise ValueError
        """

    return dog_dict


def main():
    with open("dogs.json", "r") as f:
        dog_dict = load(f)

    with open("breeds.json", "r") as f:
        breed_dict = load(f)

    dog_dict = breed_ids_to_strs(dog_dict, breed_dict)

    with open("dogs.json", "w") as f:
        dump(dog_dict, f)


if __name__ == "__main__":
    main()
