import os
import sys
import unittest
from models import db, Dogs, Breeds, Shelters


class DBTestCases(unittest.TestCase):
    def test_source_insert_1(self):
        s = Dogs(id=0, name='C++', shelter='test', img1='test', img2='test', img3='test',
                 size='test', gender='test', breed1='test', age=10, desc='test', neuter='True')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Dogs).filter_by(id=0).one()
        self.assertEqual(str(r.id), '0')
        db.session.query(Dogs).filter_by(id=0).delete()
        db.session.commit()

    def test_source_insert_2(self):
        kwargs = {
            'id': 9999,
            'name': "test_shelter",
            'website': "shelter_url",
            'email': "test@shelter.website",
            'phone': '123213',
            'address': "ut austin",
            'img_url': 'test'
        }
        s = Shelters(**kwargs)
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Shelters).filter_by(id=9999).one()
        self.assertEqual(str(r.id), '9999')
        db.session.query(Shelters).filter_by(id=9999).delete()
        db.session.commit()

    def test_source_insert_3(self):
        test_breed = {
            'id': 99999,
            'name': 'test',
            'weight_lower': 1.4,
            'weight_upper': 343,
            'height_lower': 3.8,
            'height_upper': 43,
            'age_lower': 0,
            'age_upper': 29,
            'bred_for': "Everything!",
            'breed_group': "All of them :)",
            'origin': "All over the world",
            'img1': "https://cdn2.thedogapi.com/images/B1Edfl9NX_1280.jpg",
            'img2': "https://cdn2.thedogapi.com/images/Sypubg54Q_1280.jpg",
            'img3': "https://cdn2.thedogapi.com/images/ryzzmgqE7_1280.jpg",
        }
        s = Breeds(**test_breed)
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Breeds).filter_by(id=99999).one()
        self.assertEqual(str(r.id), '99999')
        db.session.query(Breeds).filter_by(id=99999).delete()
        db.session.commit()

    def test_source_insert_4(self):
        test_breed = {
            'id': 1234,
            'name': 'test2',
            'weight_lower': 1.4,
            'weight_upper': 343,
            'height_lower': 3.8,
            'height_upper': 43,
            'age_lower': 0,
            'age_upper': 29,
            'bred_for': "Everything!",
            'breed_group': "All of them :)",
            'origin': "All over the world",
            'img1': "https://cdn2.thedogapi.com/images/B1Edfl9NX_1280.jpg",
            'img2': "https://cdn2.thedogapi.com/images/Sypubg54Q_1280.jpg",
            'img3': "https://cdn2.thedogapi.com/images/ryzzmgqE7_1280.jpg",
        }
        s = Breeds(**test_breed)
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Breeds).filter_by(id=1234).one()
        self.assertEqual(str(r.id), '1234')
        db.session.query(Breeds).filter_by(id=1234).delete()
        db.session.commit()

    def test_search_1(self):
        kwargs = {
            'id': 9999,
            'name': "first shelter",
            'website': "shelter_url",
            'email': "test@shelter.website",
            'phone': '123213',
            'address': "ut austin",
            'img_url': 'test'
        }
        s = Shelters(**kwargs)
        db.session.add(s)

        kwargs = {
            'id': 9998,
            'name': "1st shelter",
            'website': "shelter_url",
            'email': "test@shelter.website",
            'phone': '123213',
            'address': "ut austin",
            'img_url': 'test'
        }
        s2 = Shelters(**kwargs)
        db.session.add(s2)
        db.session.commit()

        shelter = (db.session.query(Shelters).filter(
            Shelters.name.ilike("%first%")).all())[0]
        self.assertEqual(str(shelter.name), "first shelter")

        db.session.query(Shelters).filter_by(id=9999).delete()
        db.session.query(Shelters).filter_by(id=9998).delete()
        db.session.commit()

    def test_search_2(self):
        test_breed = {
            'id': 1234,
            'name': 'test2',
            'weight_lower': 1.4,
            'weight_upper': 343,
            'height_lower': 3.8,
            'height_upper': 43,
            'age_lower': 0,
            'age_upper': 29,
            'bred_for': "foobar! lorem ipsum dolor amet",
            'breed_group': "All of them :)",
            'origin': "All over the world",
            'img1': "https://cdn2.thedogapi.com/images/B1Edfl9NX_1280.jpg",
            'img2': "https://cdn2.thedogapi.com/images/Sypubg54Q_1280.jpg",
            'img3': "https://cdn2.thedogapi.com/images/ryzzmgqE7_1280.jpg",
        }
        s = Breeds(**test_breed)
        db.session.add(s)
        db.session.commit()

        breed = (db.session.query(Breeds).filter(
            Breeds.bred_for.ilike("%foobar%")).all())[0]

        self.assertEqual(str(breed.id), '1234')

        db.session.query(Breeds).filter_by(id=1234).delete()
        db.session.commit()

    def test_search_3(self):
        description_text = "The quick brown fox jumps over the lazy dog"
        s = Dogs(id=0, name='C++', shelter='test', img1='test', img2='test', img3='test',
                 size='test', gender='test', breed1='test', age=10, desc=description_text, neuter='True')
        db.session.add(s)
        db.session.commit()

        dog = (db.session.query(Dogs).filter(
            Dogs.desc.ilike("%fox%")).all())[0]
        self.assertEqual(str(dog.name), 'C++')

        db.session.query(Dogs).filter_by(id=0).delete()
        db.session.commit()

    def test_dog_edge(self):
        clifford = Dogs(id=1234, name='Clifford', shelter='Emily', img1='placeholder', img2='placeholder', img3='placeholder',
                 size='test', gender='Male', breed1='BigRed', age=10, desc='He is a big red dog.', neuter='True')
        db.session.add(clifford)
        db.session.commit()
        r = db.session.query(Dogs).filter_by(id=1234).one()
        self.assertEqual(str(r.id), '1234')
        db.session.query(Dogs).filter_by(id=1234).delete()
        db.session.commit()

    def test_shelter_edge(self):
        dolittle = {
            'id': 3456,
            'name': "DR_Dolittle",
            'website': "www.dolittle.com",
            'email': "dolittle@shelter.website",
            'phone': '4695738383',
            'address': "The farm",
            'img_url': 'placeholder'
        }
        s = Shelters(**dolittle)
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Shelters).filter_by(id=3456).one()
        self.assertEqual(str(r.id), '3456')
        db.session.query(Shelters).filter_by(id=3456).delete()
        db.session.commit()

    def test_breed_edge(self):
        shibe = {
            'id': 8456,
            'name': 'shiba inu',
            'weight_lower': -10,
            'weight_upper': 40,
            'height_lower': -3,
            'height_upper': 4,
            'age_lower': 0,
            'age_upper': 29,
            'bred_for': "Hunting, cuteness and bad meme value.",
            'breed_group': "Spitz",
            'origin': "Japan.",
            'img1': "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12224412/Shiba-Inu-On-White-01.jpg",
            'img2': "https://upload.wikimedia.org/wikipedia/en/thumb/5/5f/Original_Doge_meme.jpg/300px-Original_Doge_meme.jpg",
            'img3': "https://cdn1-www.dogtime.com/assets/uploads/gallery/shiba-inu-dog-breed-picutres/8-side.jpg",
        }
        s = Breeds(**shibe)
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Breeds).filter_by(id=8456).one()
        self.assertEqual(str(r.id), '8456')
        db.session.query(Breeds).filter_by(id=8456).delete()
        db.session.commit()

    def test_value_1(self):
        s = Dogs(id=1000000, name='i', shelter='love', img1='you', img2='you', img3='love',
                 size='me', gender='were', breed1='a', age=10, desc='happy family', neuter='True')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Dogs).filter_by(id=1000000).one()
        self.assertEqual(str(r.id), '1000000')
        db.session.query(Dogs).filter_by(id=1000000).delete()
        db.session.commit()

    def test_value_2(self):
        kwargs = {
            'id': 2147483647,
            'name': "pokemon center",
            'website': "pokemonyellow.com",
            'email': "thepokecenter@pokemonyellow.com",
            'phone': '133-3337',
            'address': "victory road",
            'img_url': 'missingno'
        }
        s = Shelters(**kwargs)
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Shelters).filter_by(id=2147483647).one()
        self.assertEqual(str(r.id), '2147483647')
        db.session.query(Shelters).filter_by(id=2147483647).delete()
        db.session.commit()

    def test_value_3(self):
        test_breed = {
            'id': -2147483647,
            'name': 'test',
            'weight_lower': 0,
            'weight_upper': 100,
            'height_lower':0,
            'height_upper': 100,
            'age_lower': 0,
            'age_upper': 100,
            'bred_for': "Pikachu",
            'breed_group': "Pikachu",
            'origin': "Pokemon World",
            'img1': "https://i.imgur.com/sohWhy9.jpg",
            'img2': "https://i.imgur.com/sohWhy9.jpg",
            'img3': "https://i.imgur.com/sohWhy9.jpg",
        }
        s = Breeds(**test_breed)
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Breeds).filter_by(id=-2147483647).one()
        self.assertEqual(str(r.id), '-2147483647')
        db.session.query(Breeds).filter_by(id=-2147483647).delete()
        db.session.commit()

  ################################################################
    def test_dog_edge(self):
        clifford = Dogs(id=1234, name='Clifford', shelter='Emily', img1='placeholder', img2='placeholder', img3='placeholder',
                 size='test', gender='Male', breed1='BigRed', age=10, desc='He is a big red dog.', neuter='True')
        db.session.add(clifford)
        db.session.commit()
        r = db.session.query(Dogs).filter_by(id=1234).one()
        self.assertEqual(str(r.id), '1234')
        db.session.query(Dogs).filter_by(id=1234).delete()
        db.session.commit()

    def test_shelter_edge(self):
        dolittle = {
            'id': 3456,
            'name': "Dolittle",
            'website': "wom",
            'email': "website",
            'phone': '4695738383',
            'address': "TheFarm",
            'img_url': 'placeholder'
        }
        s = Shelters(**dolittle)
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Shelters).filter_by(id=3456).one()
        self.assertEqual(str(r.id), '3456')
        db.session.query(Shelters).filter_by(id=3456).delete()
        db.session.commit()

    def test_breed_edge(self):
        shibe = {
            'id': 8456,
            'name': 'shiba inu',
            'weight_lower': -10,
            'weight_upper': 40,
            'height_lower': -3,
            'height_upper': 4,
            'age_lower': 0,
            'age_upper': 29,
            'bred_for': "Hunting, cuteness and bad meme value.",
            'breed_group': "Spitz",
            'origin': "Japan.",
            'img1': "https://i.imgur.com/VrAWfuz.jpg",
            'img2': "https://i.imgur.com/fz0k7v8.jpg",
            'img3': "https://i.imgur.com/xDECKgM.jpg",
        }
        s = Breeds(**shibe)
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Breeds).filter_by(id=8456).one()
        self.assertEqual(str(r.id), '8456')
        db.session.query(Breeds).filter_by(id=8456).delete()
        db.session.commit()
if __name__ == '__main__':
    unittest.main()
