from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get(
    "DB_STRING", 'postgres://postgres:ot.ft.2016@localhost:5432/idb')
# to suppress a warning message
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)


class Dogs(db.Model):
    __tablename__ = 'Dogs'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(300), nullable=False)
    breed1 = db.Column(db.String(300), nullable=False)
    breed2 = db.Column(db.String(300), nullable=True)
    size = db.Column(db.String(300), nullable=False)
    gender = db.Column(db.String(300), nullable=False)
    shelter = db.Column(db.String(300), nullable=False)
    age = db.Column(db.Integer)
    img1 = db.Column(db.String(300), nullable=False)
    img2 = db.Column(db.String(300), nullable=True)
    img3 = db.Column(db.String(300), nullable=True)
    desc = db.Column(db.Text, nullable=False)
    neuter = db.Column(db.String(300), nullable=False)


class Breeds(db.Model):
    __tablename__ = 'Breeds'
    id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(80), primary_key=True)
    weight_lower = db.Column(db.Float, nullable=False)
    weight_upper = db.Column(db.Float, nullable=True)
    height_lower = db.Column(db.Float, nullable=False)
    height_upper = db.Column(db.Float, nullable=True)
    age_lower = db.Column(db.Integer, nullable=False)
    age_upper = db.Column(db.Integer, nullable=True)
    bred_for = db.Column(db.String(160), nullable=False)
    breed_group = db.Column(db.String(80), nullable=True)
    origin = db.Column(db.String(80), nullable=False)
    img1 = db.Column(db.String(300), nullable=False)
    img2 = db.Column(db.String(300), nullable=True)
    img3 = db.Column(db.String(300), nullable=True)


class Shelters(db.Model):
    __tablename__ = 'Shelters'
    id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(100), primary_key=True)
    website = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100), nullable=False)
    phone = db.Column(db.String(50), nullable=False)
    address = db.Column(db.String(150), nullable=False)
    img_url = db.Column(db.String(200), nullable=False)
    #size = db.Column(db.Integer, nullable=False)
    #capacity = db.Column(db.Integer, nullable = False)
    #about = db.Column(db.String(80), nullable=False)
    #location = db.Column(db.String(80), nullable=False)
    #hours = db.Column(db.String(80), nullable=False)
    #phone = db.Column(db.String(80), nullable=False)
    #map = db.Column(db.String(80), nullable=False)


db.create_all()  # tab in and out to create db or prevent runtime IntegrityError
# db.drop_all()
