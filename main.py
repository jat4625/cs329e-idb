import os
from flask import render_template, request, current_app
from flask_pager import Pager
from create_db import app, db, Dogs, Breeds, Shelters
from json import load
import gitlab

app.secret_key = os.urandom(42)
app.config['PAGE_SIZE'] = 16
app.config['VISIBLE_PAGE_COUNT'] = 16
# ---------------------(Search)--------------------------------


@app.route('/search/', methods=['GET', 'POST'])
def search():
    query = str(request.args['q'])
    dog_results = []
    breed_results = []
    shelter_results = []

    # Search dogs
    if query.isdigit():
        id_lookup = db.session.query(Dogs).get(int(query))
        if id_lookup:
            dog_results.append(id_lookup)
    dog_results += (db.session.query(Dogs).filter(
        Dogs.name.ilike(f"%{query}%")).all())
    dog_results += (db.session.query(Dogs).filter(
        Dogs.desc.ilike(f"%{query}%")).all())
    dog_results += (db.session.query(Dogs).filter(
        Dogs.breed1.ilike(f"%{query}%")).all())
    dog_results += (db.session.query(Dogs).filter(
        Dogs.breed2.ilike(f"%{query}%")).all())
    dog_results += (db.session.query(Dogs).filter(
        Dogs.shelter.ilike(f"%{query}%")).all())
    dog_results = sorted(list(set(dog_results)), key=lambda x: x.name)

    # Search breeds
    breed_results += (db.session.query(Breeds).filter(
        Breeds.name.ilike(f"%{query}%")).all())
    breed_results += (db.session.query(Breeds).filter(
        Breeds.bred_for.ilike(f"%{query}%")).all())
    breed_results += (db.session.query(Breeds).filter(
        Breeds.breed_group.ilike(f"%{query}%")).all())
    breed_results += (db.session.query(Breeds).filter(
        Breeds.origin.ilike(f"%{query}%")).all())
    breed_results = sorted(list(set(breed_results)), key=lambda x: x.name)

    # Search shelters
    shelter_results += (db.session.query(Shelters).filter(
        Shelters.name.ilike(f"%{query}%")).all())
    shelter_results += (db.session.query(Shelters).filter(
        Shelters.website.ilike(f"%{query}%")).all())
    shelter_results += (db.session.query(Shelters).filter(
        Shelters.email.ilike(f"%{query}%")).all())
    shelter_results += (db.session.query(Shelters).filter(
        Shelters.phone.ilike(f"%{query}%")).all())
    shelter_results += (db.session.query(Shelters).filter(
        Shelters.address.ilike(f"%{query}%")).all())
    shelter_results = sorted(list(set(shelter_results)), key=lambda x: x.name)

    return render_template("search.html", dogs=dog_results, breeds=breed_results, shelters=shelter_results)
# ---------------------(breeds)-------------------------------
@app.route('/breeds/<name>')
def breed_page(name):
    breed = db.session.query(Breeds).get(name)
    dogs = db.session.query(Dogs)
    dogs_filtered = []
    shelters = []

    for dog in dogs:
        if (dog.breed1 == name) or ((dog.breed2 == name) and (dog.breed2 is not None)):
            dogs_filtered.append(dog)
            if dog.shelter not in shelters:
                shelters.append(db.session.query(Shelters).get(dog.shelter))
    shelters = list(set(shelters))

    return render_template("breed_base.html", breed=breed, dogs=dogs_filtered, shelters=shelters)


@app.route('/breeds/')
def breeds():
    page = int(request.args.get('page', 1))
    data = db.session.query(Breeds).all()
    pager = Pager(page, db.session.query(Breeds).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("dog_breed.html", breeds=data_to_show, pages=pages)

# -----------------------(Dogs)-------------------------------
@app.route('/dogs/<dog_id>')
def dog_page(dog_id):
    """ Display an individual dog from the DB """
    """
    dog = db.session.query(Dogs).all()
    for i in dog:
        if i.id == int(id):
            breeds = []
            breed1_str = get_breed_name(i.breed1)
            breed2_str = get_breed_name(i.breed2)
            breeds.append([breed1_str, i.breed1])
            if breed2_str is not None:
                breeds.append([breed2_str, i.breed2])
            return render_template("dog_base.html", dog=i, breeds=breeds)
    """
    dog = db.session.query(Dogs).get(dog_id)
    shelter = db.session.query(Shelters).get(dog.shelter)
    breeds = []
    breeds.append(db.session.query(Breeds).get(dog.breed1))
    try:
        breeds.append(db.session.query(Breeds).get(dog.breed2))

    except AttributeError:
        pass
    except TypeError:
        pass
    return render_template("dog_base.html", dog=dog, breeds=breeds, shelter=shelter)


def get_breed_name(breed_id):
    if breed_id is not None:
        breed = db.session.query(Breeds).get(int(breed_id))
        return breed.name
    else:
        return None


@app.route('/dogs/')
def dogs():
    page = int(request.args.get('page', 1))
    data = db.session.query(Dogs).all()
    pager = Pager(page, db.session.query(Dogs).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("dogs.html", dogs=data_to_show, pages=pages)

# -----------------------(Dogsort)-------------------------------
@app.route('/sort/sortdogs_nameup/')
def dogsortnameup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Dogs).order_by(Dogs.name.asc())
    pager = Pager(page, db.session.query(Dogs).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortdogs_nameup.html", dogs=data_to_show, pages=pages)


@app.route('/sort/sortdogs_namedown/')
def dogsortnamedown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Dogs).order_by(Dogs.name.desc())
    pager = Pager(page, db.session.query(Dogs).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortdogs_namedown.html", dogs=data_to_show, pages=pages)


@app.route('/sort/sortdogs_idup/')
def dogsortidup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Dogs).order_by(Dogs.id.asc())
    pager = Pager(page, db.session.query(Dogs).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortdogs_idup.html", dogs=data_to_show, pages=pages)


@app.route('/sort/sortdogs_iddown/')
def dogsortiddown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Dogs).order_by(Dogs.id.desc())
    pager = Pager(page, db.session.query(Dogs).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortdogs_iddown.html", dogs=data_to_show, pages=pages)


@app.route('/sort/sortdogs_ageup/')
def dogsortageup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Dogs).order_by(Dogs.age.asc())
    pager = Pager(page, db.session.query(Dogs).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortdogs_ageup.html", dogs=data_to_show, pages=pages)


@app.route('/sort/sortdogs_agedown/')
def dogsortagedown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Dogs).order_by(Dogs.age.desc())
    pager = Pager(page, db.session.query(Dogs).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortdogs_agedown.html", dogs=data_to_show, pages=pages)


@app.route('/sort/sortdogs_shelterup/')
def dogsortshelterup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Dogs).order_by(Dogs.shelter.asc())
    pager = Pager(page, db.session.query(Dogs).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortdogs_shelterup.html", dogs=data_to_show, pages=pages)


@app.route('/sort/sortdogs_shelterdown/')
def dogsortshelterdown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Dogs).order_by(Dogs.shelter.desc())
    pager = Pager(page, db.session.query(Dogs).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortdogs_shelterdown.html", dogs=data_to_show, pages=pages)


@app.route('/sort/sortdogs_genderup/')
def dogsortgenderup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Dogs).order_by(Dogs.gender.desc())
    pager = Pager(page, db.session.query(Dogs).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortdogs_genderup.html", dogs=data_to_show, pages=pages)


@app.route('/sort/sortdogs_genderdown/')
def dogsortgenderdown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Dogs).order_by(Dogs.gender.asc())
    pager = Pager(page, db.session.query(Dogs).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortdogs_genderdown.html", dogs=data_to_show, pages=pages)

# -----------------------(Breedsort)-------------------------------
@app.route('/sort/sortbreeds_nameup/')
def breedsortnameup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Breeds).order_by(Breeds.name.asc())
    pager = Pager(page, db.session.query(Breeds).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortbreeds_nameup.html", breeds=data_to_show, pages=pages)


@app.route('/sort/sortbreeds_namedown/')
def breedsortnamedown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Breeds).order_by(Breeds.name.desc())
    pager = Pager(page, db.session.query(Breeds).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortbreeds_namedown.html", breeds=data_to_show, pages=pages)


@app.route('/sort/sortbreeds_weightup/')
def breedsortweightup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Breeds).order_by(Breeds.weight_upper.asc())
    pager = Pager(page, db.session.query(Breeds).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortbreeds_weightup.html", breeds=data_to_show, pages=pages)


@app.route('/sort/sortbreeds_weightdown/')
def breedsortweightdown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Breeds).order_by(Breeds.weight_upper.desc())
    pager = Pager(page, db.session.query(Breeds).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortbreeds_weightdown.html", breeds=data_to_show, pages=pages)


@app.route('/sort/sortbreeds_groupup/')
def breedsortgroupup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Breeds).order_by(Breeds.breed_group.asc())
    pager = Pager(page, db.session.query(Breeds).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortbreeds_nameup.html", breeds=data_to_show, pages=pages)


@app.route('/sort/sortbreeds_groupdown/')
def breedsortgroupdown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Breeds).order_by(Breeds.breed_group.desc())
    pager = Pager(page, db.session.query(Breeds).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortbreeds_groupdown.html", breeds=data_to_show, pages=pages)


@app.route('/sort/sortbreeds_lifespanup/')
def breedsortlifespanup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Breeds).order_by(Breeds.age_upper.asc())
    pager = Pager(page, db.session.query(Breeds).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortbreeds_lifespanup.html", breeds=data_to_show, pages=pages)


@app.route('/sort/sortbreeds_lifespandown/')
def breedsortlifespandown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Breeds).order_by(Breeds.age_upper.desc())
    pager = Pager(page, db.session.query(Breeds).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortbreeds_lifespandown.html", breeds=data_to_show, pages=pages)


@app.route('/sort/sortbreeds_originup/')
def breedsortoriginup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Breeds).order_by(Breeds.origin.asc())
    pager = Pager(page, db.session.query(Breeds).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortbreeds_originup.html", breeds=data_to_show, pages=pages)


@app.route('/sort/sortbreeds_origindown/')
def breedsortorigindown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Breeds).order_by(Breeds.origin.desc())
    pager = Pager(page, db.session.query(Breeds).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortbreeds_origindown.html", breeds=data_to_show, pages=pages)

# -----------------------(Sheltersort)-------------------------------
@app.route('/sort/sortshelters_nameup/')
def sheltersortnameup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Shelters).order_by(Shelters.name.asc())
    pager = Pager(page, db.session.query(Shelters).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortshelters_nameup.html", shelters=data_to_show, pages=pages)


@app.route('/sort/sortshelters_namedown/')
def sheltersortnamedown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Shelters).order_by(Shelters.name.desc())
    pager = Pager(page, db.session.query(Shelters).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortshelters_namedown.html", shelters=data_to_show, pages=pages)


@app.route('/sort/sortshelters_idup/')
def sheltersortidup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Shelters).order_by(Shelters.id.asc())
    pager = Pager(page, db.session.query(Shelters).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortshelters_idup.html", shelters=data_to_show, pages=pages)


@app.route('/sort/sortshelters_iddown/')
def sheltersortiddown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Shelters).order_by(Shelters.id.desc())
    pager = Pager(page, db.session.query(Shelters).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortshelters_iddown.html", shelters=data_to_show, pages=pages)


@app.route('/sort/sortshelters_emailup/')
def sheltersortemailup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Shelters).order_by(Shelters.email.asc())
    pager = Pager(page, db.session.query(Shelters).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortshelters_emailup.html", shelters=data_to_show, pages=pages)


@app.route('/sort/sortshelters_emaildown/')
def sheltersortemaildown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Shelters).order_by(Shelters.email.desc())
    pager = Pager(page, db.session.query(Shelters).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortshelters_emaildown.html", shelters=data_to_show, pages=pages)


@app.route('/sort/sortshelters_phoneup/')
def sheltersortphoneup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Shelters).order_by(Shelters.phone.asc())
    pager = Pager(page, db.session.query(Shelters).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortshelters_phoneup.html", shelters=data_to_show, pages=pages)


@app.route('/sort/sortshelters_phonedown/')
def sheltersortphonedown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Shelters).order_by(Shelters.phone.desc())
    pager = Pager(page, db.session.query(Shelters).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortshelters_phonedown.html", shelters=data_to_show, pages=pages)


@app.route('/sort/sortshelters_addressup/')
def sheltersortaddressup():
    page = int(request.args.get('page', 1))
    data = db.session.query(Shelters).order_by(Shelters.address.asc())
    pager = Pager(page, db.session.query(Shelters).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortshelters_addressup.html", shelters=data_to_show, pages=pages)


@app.route('/sort/sortshelters_addressdown/')
def sheltersortaddressdown():
    page = int(request.args.get('page', 1))
    data = db.session.query(Shelters).order_by(Shelters.address.desc())
    pager = Pager(page, db.session.query(Shelters).count())
    pages = pager.get_pages()
    skip = (page - 1) * current_app.config['PAGE_SIZE']
    limit = current_app.config['PAGE_SIZE']
    data_to_show = data[skip: skip + limit]
    return render_template("sort/sortshelters_addressdown.html", shelters=data_to_show, pages=pages)

# ---------------------(Shelters)-------------------------------
@app.route('/shelters/<name>')
def shelter_page(name):
    shelter = db.session.query(Shelters).filter_by(name=name).first_or_404()
    dogs = db.session.query(Dogs).filter(Dogs.shelter == shelter.name)
    breeds = []
    for dog in dogs:
        if dog.breed1 is not None:
            breeds.append(db.session.query(Breeds).get(dog.breed1))
        if dog.breed2 is not None:
            breeds.append(db.session.query(Breeds).get(dog.breed2))
    breeds = list(set(breeds))
    # Get the available breeds

    return render_template("shelter_base.html", shelter=shelter, dogs=dogs, breeds=breeds)


@app.route('/shelters/')
def shelters():
    shelts = db.session.query(Shelters).all()
    return render_template("shelters.html", shelters=shelts)


# ---------------------(Personal)-------------------------------
def sanitize_name(name_str):
    if name_str == 'jat4625' or name_str == 'Jose Torres':
        return 'Jose Torres'
    elif name_str == 'Sean Yu':
        return name_str
    elif name_str == 'Albert' or name_str == 'Albert Liang':
        return 'Albert Liang'
    else:
        return 'Jungwoo Cho'


def get_project_info():
    gl = gitlab.Gitlab.from_config('gitlab', ['./.python-gitlab.cfg'])
    gl.auth()

    # Setup dictionary structure
    project_info = {
        'stats': {
            'commits': 0,
            'issues': 0,
            'unit_tests': 13
        },
        'members': {}
    }

    with open('./scraped_data/about.json') as f:
        member_info = load(f)

    project_info['members'] = member_info

    # get the project
    idb = gl.projects.get(11300482)

    # Count commits
    for commit in idb.commits.list(all=True):
        author_name = sanitize_name(commit.author_name)
        project_info['members'][author_name]['commits'] += 1
        project_info['stats']['commits'] += 1

    # Count issues
    for issue in idb.issues.list(all=True):
        author_name = issue.author['name']
        project_info['members'][author_name]['issues'] += 1
        project_info['stats']['issues'] += 1

    # Manually add unit tests
    project_info['members']['Jose Torres']['unit_tests'] = 4
    project_info['members']['Sean Yu']['unit_tests'] = 3
    project_info['members']['Jungwoo Cho']['unit_tests'] = 3
    project_info['members']['Albert Liang']['unit_tests'] = 3
    return project_info


@app.route('/About')
def About():
    # Read in project info from api
    project_info = get_project_info()
    return render_template("About.html", project=project_info)


@app.route('/')
def index():
    return render_template("index.html")


if __name__ == "__main__":
    app.run()
